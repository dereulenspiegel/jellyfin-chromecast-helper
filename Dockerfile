FROM scratch

ADD jellyfin-chromecast-helper /

ENTRYPOINT ["/jellyfin-chromecast-helper"]
