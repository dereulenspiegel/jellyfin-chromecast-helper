package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"time"
)

var (
	httpAddr        = flag.String("addr", ":8181", "Listen address")
	jellyfinAddr    = flag.String("jellyfin.addr", "", "Address of the jellyfin server")
	jellyfinName    = flag.String("jellyfin.name", "Jellyfin", "Jellyfin server name")
	jellyfinVersion = flag.String("jellyfin.version", "10.6.2", "Jellyfin version")
	jellyfinID      = flag.String("jellyfin.id", "f493033cefec41388dd38783a66f50cc", "Jellyfin ID")

	defaultTimeout = time.Second * 10
)

var responseFmtString = `{"LocalAddress": "%s","ServerName": "%s","Version": "%s","ProductName": "Jellyfin Server","OperatingSystem": "Linux","Id": "%d"}`

func main() {
	flag.Parse()

	response := fmt.Sprintf(responseFmtString, *jellyfinAddr, *jellyfinName, *jellyfinVersion, *jellyfinVersion)

	handler := http.NewServeMux()
	handler.Handle("/System/Info/Public", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(response))
	}))

	server := http.Server{
		Addr:              *httpAddr,
		Handler:           handler,
		IdleTimeout:       defaultTimeout,
		ReadHeaderTimeout: defaultTimeout,
		ReadTimeout:       defaultTimeout,
		WriteTimeout:      defaultTimeout,
	}

	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatalf("HTTP server failed: %s", err)
	}
}
